import React from 'react';
import { Container, Row, Col} from 'react-bootstrap';
import { flatten, times, range } from 'lodash';
import { StickyContainer, Sticky } from 'react-sticky';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button} from 'react-bootstrap';
import logo from './logo.svg';
import './App.css';
import { callbackify } from 'util';

function App() {

  const NUMBER_OF_PARAGRAPHS = 15;
  const paragraphs = flatten(times(NUMBER_OF_PARAGRAPHS, (index) =>
    (<p key={index}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
      Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
      Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
      Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>)
  ));
  
  const NUMBER_OF_KITTIES = 1;
  const kitties = range(0,NUMBER_OF_KITTIES).map((i) => {
    return (
      <div className="sidebar-kitties" key={i}>
        <StickyContainer style={{zIndex: 2}}>
         
          <Sticky>{({style}) =><Navbar style={style} bg="light" expand="lg" >
                  <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
                  <Navbar.Toggle aria-controls="basic-navbar-nav" />
                  <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto sticky-top">
                      <Nav.Link href="#home">Home</Nav.Link>
                      <Nav.Link href="#link">Link</Nav.Link>
                      <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                      </NavDropdown>
                    </Nav>
                    <Form inline>
                      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                      <Button variant="outline-success">Search</Button>
                    </Form>
                  </Navbar.Collapse>
                </Navbar>}
          </Sticky>
          {                        <div className="justify-conten-center">{paragraphs.map(paragraph => paragraph)}</div>
}<div style={{height: "auto"}}></div>
        </StickyContainer>
      </div>
    )
  })

  return (
    <div className="App">   
      <header>
      <link
  rel="stylesheet"
  href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous"
/>
      </header>     
      <Container>
          <Row>
            <Col sm={2}>
              <StickyContainer style={{zIndex: 2, position: 'fixed'}}>
                <Sticky>{({ style }) => 
                (<div className="full-width" style={style}>
                  <Row>
                    <div>TOOL 1</div>
                  </Row>
                  <Row>
                    <div>TOOL 2</div>
                  </Row>
                  <Row>
                    <div>TOOL 3</div>
                  </Row>
                  <Row>
                    <div>TOOL 4</div>
                  </Row>
            
                  </div> )
                }
                </Sticky>
              </StickyContainer>
            </Col>

            <Col sm={6}> 
            {kitties}
            
            </Col>

            <Col sm={4}>
            <StickyContainer style={{zIndex: 2, position: 'fixed'}}>
            <Sticky>{({ style }) => 
            <h1 style={style}>Sticky element</h1>}
              </Sticky>
              </StickyContainer>
            
            </Col>
          </Row>
      </Container>
    </div>
  );
}

export default App;
